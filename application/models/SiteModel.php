<?php

namespace application\models;

use application\components\Model;
use application\components\Curl;

class SiteModel extends Model
{
    /**
     *
     * @var array 
     */
    private $_regExpConfig = array(
        1 => array(
            'idx' => 2,
            'exp' => '/href=(\'|")(.*?)(\'|")/si',
        ),
        2 => array(
            'idx' => 1,
            'exp' => '/<img\s+.*?src=[\"\']?([^\"\' >]*)[\"\']?[^>]*>/i',
        ),
        3 => array(
            'idx' => 0,
            'exp' => '/{text}/i',
        ),
    );

    /**
     * 
     * @param null|string $error
     * @return boolean
     */
    public function processData(&$error)
    {
        $data = array(
            'url' => htmlspecialchars($_POST['searchUrl']),
            'type' => htmlspecialchars($_POST['searchType']),
            'text' => htmlspecialchars($_POST['searchText']),
        );
        
        if(empty($data['url']) || empty($data['type']) || ($data['type'] == 3 && empty($data['text']))){
            $error = 'Данные не валидны';
            return false;
        }

        $curl = new Curl($data['url']);
        $curl->request();

        $content = $curl->getResult();
        
        if($content === false){
            $error = 'Ошибка при запросе к ресурсу';
            return false;
        }
        
        $regConfig = $this->_regExpConfig[(int)$data['type']];
        if($data['type'] == 3) $regConfig['exp'] = str_replace('{text}', $data['text'], $regConfig['exp']);
        
        $matches = array();
        preg_match_all($regConfig['exp'], $content, $matches);
        
        $result = $matches[$regConfig['idx']];
        
        try {
            $query = $this->getConnection()->prepare('INSERT INTO `links` (`url`, `matches`, `count`) VALUES (:url, :matches, :count)');
            $query->bindValue(':url', $data['url']);
            $query->bindValue(':matches', implode(', ', $result));
            $query->bindValue(':count', sizeof($result));
            $query->execute();
        } catch(\PDOException $e){
            $error = 'Ошибка при записи в БД';
            return false;
        }
        
        return true;
    }
    
    /**
     * 
     * @return array|bool
     */
    public function getData()
    {
        try {
            $query = $this->getConnection()->query('SELECT `id`, `url`, `count` FROM `links`');
            return $query->fetchAll();
        } catch(\PDOException $e){
            return false;
        }
    }
    
    /**
     * 
     * @return array|bool
     */
    public function getDataDetails()
    {
        try {
            $query = $this->getConnection()->prepare('SELECT `matches` FROM `links` WHERE `id` = ?');
            $query->execute(array($_GET['id']));
            $data = $query->fetch();
            return !empty($data) ? explode(', ', $data['matches']) : array();
        } catch(\PDOException $e){
            return false;
        }
    }
}
