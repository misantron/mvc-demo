<?php

namespace application\components;

class View
{
    public $layout;

    function __construct($layout)
    {
        $this->layout = $layout;
    }

    public function render($template, $data = array())
    {
        include VIEWS_PATH . 'layouts' . DIRECTORY_SEPARATOR . $this->layout;
    }
}
