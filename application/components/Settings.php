<?php

namespace application\components;

class Settings
{
    protected static $_data = array();
    
    public static function get($name)
    {
        return isset(static::$_data[$name]) ? static::$_data[$name] : null;
    }
    
    public static function set($name, $data)
    {
        static::$_data[$name] = $data;
    }
}
