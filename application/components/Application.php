<?php

namespace application\components;

class Application
{
    const CONFIG_FILE = 'config';

    private $_controller;
    private $_action;

    public function __construct()
    {
        $this->_controller = 'Site';
        $this->_action = 'Index';

        $config = require_once(APPLICATION_PATH . self::CONFIG_FILE . '.php');
        Settings::set('config', $config);
    }

    public function run()
    {
        $this->parseUrlString();

        $modelName = 'application\models\\' . ucfirst($this->_controller) . 'Model';
        $controllerName = 'application\controllers\\' . ucfirst($this->_controller) . 'Controller';
        $action = 'action' . ucfirst($this->_action);

        /**
         * @var Controller $controller
         */
        $controller = new $controllerName;
        $controller->init($modelName);

        if(method_exists($controller, $action)){
            $controller->{$action}();
        } else {
            $controller->error(404);
        }
    }

    protected function parseUrlString()
    {
        $url = filter_var($_SERVER['REQUEST_URI'], FILTER_SANITIZE_URL);
        $routes = explode('/', $url);

        if (!empty($routes[1])) {
            $this->_controller = $routes[1];
        }

        if (!empty($routes[2])) {
            $this->_action = $routes[2];
        }
    }
}