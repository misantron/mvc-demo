<?php

namespace application\components;

abstract class Controller
{
    /**
     * @var Model
     */
    public $model;
    public $view;

    public $layout;
    
    function __construct()
    {
        $this->view = new View($this->layout);
    }

    public function init($modelName)
    {
        $this->model = new $modelName;
    }
    
    abstract public function actionIndex();
    
    public function error($code)
    {
        $this->view->render('error', array('code' => $code));
        exit;
    }

    protected function _isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
    }

    /**
     * @param array $response
     */
    protected function _ajaxResponse($response)
    {
        header('Content-type: application/json');
        $encoded = json_encode($response);
        exit($encoded);
    }
}