<?php

namespace application\components;

class Model
{
    /**
     * @var \PDO
     */
    protected $_connection;
    protected $_options = array();

    function __construct()
    {
        $this->_initConnection();
    }
    
    private function _initConnection()
    {
        if(!($this->_connection instanceof \PDO)){
            $settings = Settings::get('config');
            $dbSettings = $settings['db'];
            $dsn = sprintf('%s:host=%s;dbname=%s', $dbSettings['adapter'], $dbSettings['params']['host'], $dbSettings['params']['dbname']);

            try {
                $this->_connection = new \PDO($dsn, $dbSettings['params']['username'], $dbSettings['params']['password'], $this->_options);
                $this->_connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            } catch(\PDOException $e){
                echo $e->getMessage();
                exit;
            }
        }
    }
    
    public function getConnection()
    {
        return $this->_connection;
    }
}