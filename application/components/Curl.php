<?php

namespace application\components;

class Curl
{
    private $_handler;
    private $_options;

    private $_result;
    private $_errorCode;
    private $_errorMessage;

    public function __construct($url)
    {
        $this->_handler = curl_init($url);

        $this->_options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 5,
        );
        curl_setopt_array($this->_handler, $this->_options);
    }

    public function request()
    {
        $this->_result = curl_exec($this->_handler);
        $this->_errorCode = curl_errno($this->_handler);
        $this->_errorMessage = curl_error($this->_handler);

        curl_close($this->_handler);
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function getErrorCode()
    {
        return $this->_errorCode;
    }

    public function getErrorMessage()
    {
        return $this->_errorMessage;
    }
}