<?php

namespace application\controllers;

use application\components\Controller;

class SiteController extends Controller
{
    public $layout = 'main.php';

    public function actionIndex()
    {        
        $this->view->render('index');
    }
    
    public function actionResults()
    {
        $data = $this->model->getData();
        
        $this->view->render('results', array('list' => $data));
    }
    
    public function actionDetails()
    {
        $data = $this->model->getDataDetails();
        if(empty($data)){
            $this->error(404);
        }
        
        $this->view->render('details', array('list' => $data));
    }

    public function actionProcessData()
    {
        if($this->_isAjax()){
            $result = $this->model->processData($error = null);

            $this->_ajaxResponse(array(
                'result' => $result,
                'message' => $result ? 'Данные сайта обработаны' : $error,
            ));
        }
        $this->error(401);
    }
}
