<form id="search-form" class="form-horizontal" method="post" novalidate="novalidate" role="form">
    <div class="alert hidden" role="alert"></div>
    <div class="form-group">
        <label for="searchUrl" class="col-sm-2 control-label">Адрес сайта:</label>
        <div class="col-sm-10">
            <input type="text" name="searchUrl" class="form-control" id="searchUrl" placeholder="http://">
        </div>
    </div>
    <div class="form-group">
        <label for="form-type" class="col-sm-2 control-label">Искомый элемент:</label>
        <div class="col-sm-5">
            <label class="radio-inline">
                <input type="radio" name="searchType" id="form-type-1" value="1" checked> Ссылка
            </label>
            <label class="radio-inline">
                <input type="radio" name="searchType" id="form-type-2" value="2"> Изображение
            </label>
            <label class="radio-inline">
                <input type="radio" name="searchType" id="form-type-3" value="3"> Текст
            </label>
        </div>
    </div>
    <div id="form-text-row" class="form-group hidden">
        <label for="form-text" class="col-sm-2 control-label">Текст:</label>
        <div class="col-sm-10">
            <input type="text" name="searchText" class="form-control" id="searchText">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-2"></div>
        <div class="col-sm-5">
            <input type="submit" class="btn btn-success" value="Выполнить поиск">
            <input type="button" class="btn btn-default" value="Сбросить">
        </div>
    </div>
</form>