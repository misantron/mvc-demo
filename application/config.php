<?php

return array(
    'db' => array(
        'adapter' => 'mysql',
        'params' => array(
            'host' => 'localhost',
            'dbname' => 'mvc',
            'username' => 'root',
            'password' => '',
        ),
    ),
);