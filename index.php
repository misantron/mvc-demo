<?php

define('APPLICATION_PATH', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'application') . DIRECTORY_SEPARATOR);
define('VIEWS_PATH', APPLICATION_PATH . 'views' . DIRECTORY_SEPARATOR);

// auto load class files
spl_autoload_extensions('.php');
spl_autoload_register();

use application\components\Application;

// create and run application
$app = new Application();
$app->run();