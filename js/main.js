$(function() {
        $('input:radio').change(function(){
        $('#form-text').empty();
        $('#form-text-row').addClass('hidden');
        if(parseInt($(this).val()) == 3){
            $('#form-text-row').removeClass('hidden');
        }
    });

    $('.btn-default').click(function(){
        $('.alert')
            .removeClass('alert-success')
            .removeClass('alert-danger')
            .empty()
            .addClass('hidden')

        $('#form-type-1').prop('checked', true);
        $('input:text').val('');
        $('#form-text-row').addClass('hidden');
    });

    $('#search-form').validate({
        debug: true,
        submitHandler: function(form) {
            $.ajax({
                url: '/site/processData',
                method: 'POST',
                data: $(form).serializeArray(),
                dataType: 'json',
                success: function(response){
                    $('.alert')
                        .text(response.message)
                        .addClass(response.result ? 'alert-success' : 'alert-danger')
                        .removeClass('hidden');
                }
            });
            return false;
        },
        rules: {
            searchUrl: {
                required: true,
                url: true
            },
            searchText: {
                required: function(element){
                    return $('#form-type-3:checked');
                }
            }
        },
        messages: {
            searchUrl: 'Поле является обязательным для заполнения',
            searchText: 'Поле является обязательным для заполнения',
            url: 'Адрес сайта должен быть валидным'
        }
    });
});